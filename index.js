// Định nghĩa khi tạo 1 con server
// yarn init
// yarn add express dotenv nodemon
const express = require("express");
const app = express();
const { PrismaClient } = require("@prisma/client");
app.use(express.json());
app.use(express.static("."));
app.listen(8080);
const prisma = new PrismaClient(); // tương tự const model = inital-Model();sequelize
app.get("/demo/:id", async (req, res) => {
  const { id } = req.params;
  let data = await prisma.food.findMany({
    include: { food_type: true },
    where: {
      // food_name: {
      //   contains: "w", //WHERE food_name LIKE %W%
      // },
      food_id: Number(id), // chú ý datatype truyen vao phai dung voi datatyle model da khai bao
    },
  }); // sequelize == findAll nhưng prisma là .findMany()
  res.send(data);
});
app.post("/createFood", async (req, res) => {
  let { food_name, image, price, desc, type_id } = req.body;
  await prisma.food.create({
    data: { food_name, image, price, desc, type_id },
  });
  await prisma.food.update({
    data: { food_name, image, price, desc, type_id },
    where: { food_id: 1 },
  });
});
